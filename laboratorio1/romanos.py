#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Laboratorio 1
Para nuestra fortuna, los números romanos aún son utilizados para algunos
propósitos, por último para hacer pensar a los estudiantes de programación.
Los símbolos básicos y sus equivalencias decimales son:
| M |	1000 |
| D |	500  |
| C |	100  |
| L |	50   |
| X |	10   |
| V |	5    |
| I |	1    |

Los enteros romanos se escriben de acuerdo a las siguientes reglas:
 * Si una letra está seguida inmediatamente por una de igual o menor valor,
 su valor se suma al total acumulado. Así, XX = 20, XV = 15 y VI = 6.
 * Si una letra está seguida inmediatamente por una de mayor valor, su valor
 se sustrae del total acumulado. Así, IV = 4, XL = 40 y CM = 900.

Escriba la función romano_a_arabigo que reciba un string con un número en
notación romana, y entregue el entero equivalente

Escriba otra funcion arabigo_a_romano que reciba un entero y lo entregue con
notación romana
"""


NUMEROS_ROMANOS = {1000: 'M',
                    900: 'CM',
                    500: 'D',
                    400: 'CD',
                    100: 'C',
                     90: 'XC',
                     50: 'L',
                     40: 'XC',
                     10: 'X',
                     9: 'IX',
                     5: 'V',
                     4: 'IV',
                     1: 'I'}


def romano_a_arabigo(numero):
    #Se ordena el diccionario y se obtiene un tipo de lista
    ordenado = sorted((NUMEROS_ROMANOS.keys()))
    valores = NUMEROS_ROMANOS.values()
    #Revise el comportamiento descomentando las siguientes lineas
    # print(cc)
    # print(ordenado)
    # print(type(ordenado))
    ordenado.reverse() #Se ordena la lista de mayor a menor
    # print(ordenado)

    texto_romano = ''

    for i in ordenado:
        while i <= numero:
            texto_romano = texto_romano + NUMEROS_ROMANOS[i]
            numero = numero - i
    return texto_romano

def arabigo_a_romano(num_romano):
    arabigo=0
    i=0
    while(i<len(num_romano)):
        #Recorre en x,y los items del diccionario
        for key, value in NUMEROS_ROMANOS.items():
            #Revisa caso de 4 y 9
            if(value == num_romano[i]+num_romano[i-1]):
                arabigo=arabigo+key
                i+=1
            elif(value==num_romano[i]):
                arabigo+=key
        i+=1
    return arabigo


print(romano_a_arabigo(1930))
print(arabigo_a_romano(romano_a_arabigo(1930)))
print(romano_a_arabigo(1939))
print(arabigo_a_romano(romano_a_arabigo(1939)))
print(romano_a_arabigo(11))
print(arabigo_a_romano(romano_a_arabigo(1939)))
