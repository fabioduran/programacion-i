class Ejemplo:
    atributo_uno = 'Hola'
    atributo_dos = 'Mundo'
    # MALA PRACTICA!
    # print(atributo_uno)
    # print(atributo_dos)

    def __init__(self):
        print("constructor")
        self.metodo_uno()
        print(self.atributo_uno + " " + self.atributo_dos)
        self.metodo_dos(par2="PARAMETROS DOS", par="PARAMETRO UNO")

    def set_atributo_uno(self, nombre="Elizabeth"):
        self.atributo_uno = nombre

    def get_atributo_uno(self):
        return self.atributo_uno

    def metodo_uno(self):
        print("metodo_uno")

    def metodo_dos(self, par=0, par2="mundo"):
        print(str(par) + " " + par2)


if __name__ == '__main__':
    print(__name__)
    e = Ejemplo()
    j = Ejemplo()
    e.set_atributo_uno("Nicole")
    et = e.get_atributo_uno()
    print("Ejemplo de getter ", et)
    j.set_atributo_uno("Juanito")
    jt = j.get_atributo_uno()
    print("Ejemplo de getter ", jt)
    # obj2 = Ejemplo()
    # obj2.metodo_uno()
