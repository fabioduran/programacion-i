#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Se importa la librería Gobject Instrospection (gi)
import gi
# Valida que la versión de GTK a trabajar sea la 3
gi.require_version('Gtk', '3.0')
# importamos Gtk
from gi.repository import Gtk


class Ejemplo():

    def __init__(self):
        print("Constructor")
        # Creamos un objeto builder para manipular Gtk
        self.builder = Gtk.Builder()
        # Agregamos los objetos Gtk diseñados en Glade
        self.builder.add_from_file("ejemplo.glade")
        # Asociamos a atributos cada uno de los elementos del glade (ID)
        window = self.builder.get_object("window1")
        # Seteamos la objeto ventana (Gtk.Window) un valor de defecto
        window.set_default_size(600, 400)
        # window.fullscreen()
        # Creamos un evento para que se destruya la ventana al cerrar
        window.connect("destroy", Gtk.main_quit)

        # cargo boton con ID btnInformacion
        self.boton_informacion = self.builder.get_object("btnInformacion")
        # creo evento "clicked" para boton_informacion
        # y vinculo a método click_button
        self.boton_informacion.connect("clicked", self.click_button)

        # cargo boton con ID btnCerrar
        self.boton_cerrar = self.builder.get_object("btnCerrar")
        # creo para boton_cerrar un evento "clicked"
        # el evento se vincula a Gtk.main_quit para destruir la ventana
        self.boton_cerrar.connect("clicked", Gtk.main_quit)

        # Muestro todos los elementos de la Ventana
        window.show_all()

    # Metodo
    def click_button(self, btn=None):
        print("Presionó el Botón")

    # Metodo
    def on_window1_destroy(self, object, data=None):
        print("quit with cancel")
        Gtk.main_quit()


if __name__ == "__main__":
    # Instancio la clase Ejemplo
    w = Ejemplo()
    # Crea iteracion que maneja eventos en GTK
    Gtk.main()
