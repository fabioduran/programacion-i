#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Se importa la librería Gobject Instrospection (gi)
import gi
# Valida que la versión de GTK a trabajar sea la 3
gi.require_version('Gtk', '3.0')
# importamos Gtk
from gi.repository import Gtk
# importamos Json
import json


# Método para abrir archivo esrudianes.json
def open_file():
    try:
        with open('estudiantes.json', 'r') as file:
            # Obtenemos en la variable data todo el contenido del archivo
            data = json.load(file)
        file.close()
    except IOError:
        # Si no encontramos el archivo, se devuelve una lista vacia
        data = []

    # Retornamos los datos
    return data


# Método para guardar archivo con formato Json
def save_file(data):
    print("Save File")

    # Guardamos todos los datos enviados en 'data' en formato Json
    with open('estudiantes.json', 'w') as file:
        json.dump(data, file, indent=4)
    file.close()


class Ejemplo():

    def __init__(self):
        print("Constructor")
        # Creamos un objeto builder para manipular Gtk
        self.builder = Gtk.Builder()
        # Agregamos los objetos Gtk diseñados en Glade
        self.builder.add_from_file("ejemplo3.glade")
        # Asociamos a atributos cada uno de los elementos del glade (ID)
        window = self.builder.get_object("window")
        # Creamos un evento para que se destruya la ventana al cerrar
        window.connect("destroy", Gtk.main_quit)
        # Seteamos la objeto ventana (Gtk.Window) un valor de defecto
        window.set_default_size(600, 400)
        # Seteamos el titulo de la ventana con un valor por defecto
        window.set_title("Ejemplo de Programación I")

        # Botones
        # Cargamos el boton con ID buttonAdd
        self.button_open_dialog = self.builder.get_object("buttonAdd")
        # Creamos evento "clicked" para button_open_dialog
        # y se vincula a método open_dialog
        self.button_open_dialog.connect("clicked", self.open_dialog)

        # Cargamos el boton con ID buttonDelete
        self.button_delete = self.builder.get_object("buttonDelete")
        # Creamos evento "clicked" para button_delete
        # y se vincula a método delete_select_data
        self.button_delete.connect("clicked", self.delete_select_data)
        # Cargamos el boton con ID buttonEdit
        self.button_edit = self.builder.get_object("buttonEdit")
        # Creamos evento "clicked" para button_edit
        # y se vincula a método edit_select_data
        self.button_edit.connect("clicked", self.edit_select_data)

        # Seteamos la vista de los 4 campos que se muestran en la venta
        # cada campo una columna distinta (rut, nombre, apellido, matricula)
        self.listmodel = Gtk.ListStore(str, str, str, str)
        # Cargamos la ventana que desplega los datos almacenados
        self.treeResultado = self.builder.get_object("treeResultado")
        # Dicha venta se setea con las 4 columnas de listmodel
        self.treeResultado.set_model(model=self.listmodel)

        # Creamos (en espaciado) cada una de las columnas
        cell = Gtk.CellRendererText()
        # Seteamos los nombres de cada campo
        title = ("Rut", "Nombre", "Apellido", "Matricula")
        for i in range(len(title)):
            col = Gtk.TreeViewColumn(title[i], cell, text=i)
            self.treeResultado.append_column(col)

        self.show_all_data()

        window.show_all()

    # Método en caso de clickear boton "add"
    def open_dialog(self, btn=None):
        print("Aprete el boton add")
        d = DialogoEstudiante()
        response = d.dialogo.run()

        # En caso de apretar Ok los datos correspondientes al estudiante
        # se guardarán
        if response == Gtk.ResponseType.OK:
            print("Aprete OK")
            self.remove_all_data()
            self.show_all_data()
        # En caso contrario se cerrará la ventana desplegada para esta acción
        elif response == Gtk.ResponseType.CANCEL:
            print("Aprete Cancelar")

    # Método para mostrar todos los datos almacenados
    def show_all_data(self):
        # LLamamos a método que permite obtener archivo json
        data = open_file()
        # Obtenmos cada fila correspodiente a cada estudiante
        for i in data:
            # Se obtiene cada valor para los campos solicitados(rut, nombre..)
            x = [x for x in i.values()]
            # Seteamos la vista para cada uno de los campos
            self.listmodel.append(x)

    # Método a descubir por ustedes mismos.
    def delete_select_data(self, btn=None):
        model, it = self.treeResultado.get_selection().get_selected()
        if model is None or it is None:
            return

        data = open_file()
        for i in data:
            if(i['rut'] == model.get_value(it, 0)):
                data.remove(i)
        save_file(data)

        self.remove_all_data()
        self.show_all_data()

    # Edit data
    def edit_select_data(self, btn=None):
        model, it = self.treeResultado.get_selection().get_selected()
        if model is None or it is None:
            return
            
        d = DialogoEstudiante()
        d.rut.set_text(model.get_value(it, 0))
        d.nombre.set_text(model.get_value(it, 1))
        d.apellido.set_text(model.get_value(it, 2))
        d.matricula.set_text(model.get_value(it, 3))

        response = d.dialogo.run()

        if response == Gtk.ResponseType.OK:
            print("Aprete OK")
            self.remove_all_data()
            self.show_all_data()
        elif response == Gtk.ResponseType.CANCEL:
            print("Aprete Cancelar")

    # Método para remover todos los datos
    def remove_all_data(self):
        # Verificamos su aún hay datos en el modelo
        if len(self.listmodel) != 0:
            # remove all the entries in the model
            # Removemos todas en entradas (datos) en el modelo
            for i in range(len(self.listmodel)):
                iter = self.listmodel.get_iter(0)
                self.listmodel.remove(iter)
        # Printea un mensaje en la terminal alertando que el modelo es vacio
        print("Empty list")


# Clase que desplega menú para añadir estudiantes
class DialogoEstudiante():

    def __init__(self):
        print("Constructor")
        # Creamos un objeto builder para manipular Gtk
        self.builder = Gtk.Builder()
        # Agregamos los objetos Gtk diseñados en Glade
        self.builder.add_from_file("ejemplo3.glade")
        # Cargamos boton con ID dialogEstudiante
        self.dialogo = self.builder.get_object("dialogoEstudiante")
        # Cargamos el contenido
        self.dialogo.show_all()
        # Cargamos boton con ID buttonCance
        self.button_cancel = self.builder.get_object("buttonCance")
        # Creamos evento "clicked" para button_cancel
        # y se vincula a método button_cancel_click
        self.button_cancel.connect("clicked", self.button_cancel_clicked)
        # Cargamos boton con ID buttonOk
        self.button_ok = self.builder.get_object("buttonOk")
        # Creamos evento "Clicked para buttonOk"
        # y se vincula a método button_ok_clicked
        self.button_ok.connect("clicked", self.button_ok_clicked)

        # Cargamos los inputs entryRut, entryNombre, entryApellido,
        # entryMatricula para el ingreso de un estudiante
        self.rut = self.builder.get_object("entryRut")
        self.nombre = self.builder.get_object("entryNombre")
        self.apellido = self.builder.get_object("entryApellido")
        self.matricula = self.builder.get_object("entryMatricula")

    # Método para guardar la información correspondiente a un estudiante
    def button_ok_clicked(self, btn=None):
        # Variable rut tomará el valor que se tenga en el input rut
        rut = self.rut.get_text()
        # Variable nombre tomará el valor que se tenga en el input nombre
        nombre = self.nombre.get_text()
        # variable apellido tomará el valor que se tenga en el input apellido
        apellido = self.apellido.get_text()
        # Variable matricula tomará el valor que se tenga en el input matricula
        matricula = self.matricula.get_text()

        # Creamos diccionario con la nueva información del estudiante agregado
        j = {"rut": rut,
              "nombre": nombre,
              "apellido": apellido,
              "matricula": matricula}

        # Llamamos método open_file() y alojamos en f todo su actual contenido
        f = open_file()
        # Agregamos al nuevo estudiante al archivo json que guarda la info
        f.append(j)
        # Una ve agregado el estudiante, guardamos el archivo
        save_file(f)
        # Destrimos la actual ventana para añadir un estudiante
        self.dialogo.destroy()

    # Método destruir la ventana en caso de ser presionado boton cancel
    def button_cancel_clicked(self, btn=None):
        self.dialogo.destroy()


# Main
if __name__ == "__main__":
    # Instancio la clase Ejemplo
    p = Ejemplo()
    # Crea iteracion que maneja eventos en GTK
    Gtk.main()
