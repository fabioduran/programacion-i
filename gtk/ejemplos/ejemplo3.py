#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Se importa la librería Gobject Instrospection (gi)
import gi
# Valida que la versión de GTK a trabajar sea la 3
gi.require_version('Gtk', '3.0')
# importamos Gtk
from gi.repository import Gtk

import json


def open_file():
    try:
        with open('estudiantes.json', 'r') as file:
            data = json.load(file)
        file.close()
    except IOError:
        data = []
    return data


def save_file(data):
    print("Save File")

    with open('estudiantes.json', 'w') as file:
        json.dump(data, file, indent=4)
    file.close()


class Ejemplo():

    def __init__(self):
        print("Constructor")
        # Creamos un objeto builder para manipular Gtk
        self.builder = Gtk.Builder()
        # Agregamos los objetos Gtk diseñados en Glade
        self.builder.add_from_file("ejemplo3.glade")

        window = self.builder.get_object("window")
        window.connect("destroy", Gtk.main_quit)
        window.set_default_size(600, 400)
        window.set_title("Ejemplo de Programación I")

        self.button_open_dialog = self.builder.get_object("buttonAdd")
        self.button_open_dialog.connect("clicked", self.open_dialog)

        self.button_delete = self.builder.get_object("buttonDelete")
        self.button_delete.connect("clicked", self.delete_select_data)

        self.button_edit = self.builder.get_object("buttonEdit")
        self.button_edit.connect("clicked", self.edit_select_data)

        self.listmodel = Gtk.ListStore(str, str, str, str)
        self.treeResultado = self.builder.get_object("treeResultado")
        self.treeResultado.set_model(model=self.listmodel)

        # columns is created
        cell = Gtk.CellRendererText()
        title = ("Rut", "Nombre", "Apellido", "Matricula")
        for i in range(len(title)):
            col = Gtk.TreeViewColumn(title[i], cell, text=i)
            self.treeResultado.append_column(col)

        self.show_all_data()

        window.show_all()

    def open_dialog(self, btn=None):
        print("Aprete el boton add")
        d = DialogoEstudiante()
        response = d.dialogo.run()

        if response == Gtk.ResponseType.OK:
            print("Aprete OK")
            self.remove_all_data()
            self.show_all_data()
        elif response == Gtk.ResponseType.CANCEL:
            print("Aprete Cancelar")

    def show_all_data(self):
        data = open_file()
        for i in data:
            x = [x for x in i.values()]
            self.listmodel.append(x)

    def delete_select_data(self, btn=None):
        model, it = self.treeResultado.get_selection().get_selected()
        if model is None or it is None:
            return

        data = open_file()
        for i in data:
            if(i['rut'] == model.get_value(it, 0)):
                data.remove(i)
        save_file(data)

        self.remove_all_data()
        self.show_all_data()

    def edit_select_data(self, btn=None):
        d = DialogoEstudiante()

        model, it = self.treeResultado.get_selection().get_selected()
        if model is None or it is None:
            return
        d.rut.set_text(model.get_value(it, 0))
        d.nombre.set_text(model.get_value(it, 1))
        d.apellido.set_text(model.get_value(it, 2))
        d.matricula.set_text(model.get_value(it, 3))

        response = d.dialogo.run()

        if response == Gtk.ResponseType.OK:
            print("Aprete OK")
            self.remove_all_data()
            self.show_all_data()
        elif response == Gtk.ResponseType.CANCEL:
            print("Aprete Cancelar")
            # d.dialogo.destroy()

    def remove_all_data(self):
        # if there is still an entry in the model
        if len(self.listmodel) != 0:
            # remove all the entries in the model
            for i in range(len(self.listmodel)):
                iter = self.listmodel.get_iter(0)
                self.listmodel.remove(iter)
        # print a message in the terminal alerting that the model is empty
        print("Empty list")


class DialogoEstudiante():

    def __init__(self):
        print("Constructor")
        # Creamos un objeto builder para manipular Gtk
        self.builder = Gtk.Builder()
        # Agregamos los objetos Gtk diseñados en Glade
        self.builder.add_from_file("ejemplo3.glade")

        self.dialogo = self.builder.get_object("dialogoEstudiante")
        self.dialogo.show_all()

        self.button_cancel = self.builder.get_object("buttonCance")
        self.button_cancel.connect("clicked", self.button_cancel_clicked)

        self.button_ok = self.builder.get_object("buttonOk")
        self.button_ok.connect("clicked", self.button_ok_clicked)

        self.rut = self.builder.get_object("entryRut")
        self.nombre = self.builder.get_object("entryNombre")
        self.apellido = self.builder.get_object("entryApellido")
        self.matricula = self.builder.get_object("entryMatricula")

    def button_ok_clicked(self, btn=None):
        # save_file()
        rut = self.rut.get_text()
        nombre = self.nombre.get_text()
        apellido = self.apellido.get_text()
        matricula = self.matricula.get_text()

        j = {"rut": rut,
              "nombre": nombre,
              "apellido": apellido,
              "matricula": matricula}
        f = open_file()
        f.append(j)
        save_file(f)

        self.dialogo.destroy()

    def button_cancel_clicked(self, btn=None):
        self.dialogo.destroy()


if __name__ == "__main__":
    p = Ejemplo()
    Gtk.main()
