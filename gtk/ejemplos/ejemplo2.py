#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Se importa la librería Gobject Instrospection (gi)
import gi
# Valida que la versión de GTK a trabajar sea la 3
gi.require_version('Gtk', '3.0')
# importamos Gtk
from gi.repository import Gtk


class Ejemplo():

    def __init__(self):
        print("Constructor")
        # Creamos un objeto builder para manipular Gtk
        self.builder = Gtk.Builder()
        # Agregamos los objetos Gtk diseñados en Glade
        self.builder.add_from_file("ejemplo2.glade")

        # Ventana
        # Asociamos a atributos cada uno de los elementos del glade (ID)
        window = self.builder.get_object("window")
        # Seteamos la objeto ventana (Gtk.Window) un valor de defecto
        window.set_default_size(600, 400)
        # window.fullscreen()
        # Creamos un evento para que se destruya la ventana al cerrar
        window.connect("destroy", Gtk.main_quit)

        # botones
        # cargo boton con ID botonImprimeTexto
        self.boton_imprime_texto = self.builder.get_object("botonImprimeTexto")
        # creo evento "clicked" para boton_informacion
        # y vinculo a método click_button
        self.boton_imprime_texto.connect("clicked", self.imprime_texto)
        # cargo boton con ID botonResetResultado y asocio evento click
        self.boton_reset_resultado = self.builder.get_object("botonResetResultado")
        self.boton_reset_resultado.connect("clicked", self.reset_resultado)
        # Abre dialogo
        self.boton_nuevo_dialogo = self.builder.get_object("abreDialogo")
        self.boton_nuevo_dialogo.connect("clicked", self.nuevo_dialogo)

        # Textos
        # cargo elemento de cuadro con ID entradaTexto
        self.entrada_texto = self.builder.get_object("entradaTexto")
        # Creo evento activate para que efectúe una acción al presionar
        # la tecla ENTER o RETURN
        self.entrada_texto.connect("activate", self.imprime_texto)

        self.label_resultado = self.builder.get_object("labelResultado")

        # Muestro todos los elementos de la Ventana
        window.show_all()

    # Método
    def imprime_texto(self, btn=None):
        # Obtengo el texto del Gtk.Entry para la entrada de texto
        texto = self.entrada_texto.get_text()
        # Posiciona que toda acción realizada vuelva a estar centrada en el
        # cuadro de texto, focus
        self.entrada_texto.grab_focus_without_selecting()

        self.entrada_texto.set_text("")

        # Obtengo los textos del objeto Gtk.label de Resultado
        texto_label = self.label_resultado.get_text()
        # Concateno el texto obtenido
        texto_label += '\n' + texto
        # Inserto el texto concatenado al Gtk.Label de Resultado
        self.label_resultado.set_text(texto_label)
        print(texto)

    # Método
    def reset_resultado(self, btn=None):
        # Set el valor por defecto para el label
        self.label_resultado.set_text("Resultado:")

    # Metodo
    def nuevo_dialogo(self, btn=None):
        d = EjemploDialog()
        response = d.dialogo.run()

        if response == Gtk.ResponseType.OK:
            print("Acción OK")

        elif response == Gtk.ResponseType.CANCEL:
            print("Accion Cancelar")

    # Método
    def on_window1_destroy(self, object, data=None):
        print("quit")
        Gtk.main_quit()


class EjemploDialog():

    def __init__(self):
        # self.builder = Gtk.Builder()
        # # Agregamos los objetos Gtk diseñados en Glade
        # self.builder.add_from_file("ejemplo2.glade")
        self.builder = Gtk.Builder()
        self.builder.add_from_file("ejemplo2.glade")
        self.dialogo = self.builder.get_object("dialogo")
        # self.dialogo.
        self.dialogo.show_all()

        self.boton_ok = self.builder.get_object("botonOk")
        # self.boton_ok.connect("clicked", Gtk.ResponseType.OK)
        self.boton_cancel = self.builder.get_object("botonCancel")
        self.boton_cancel.connect("clicked", self.close_dialog)

    def close_dialog(self, obj=None):
        self.dialogo.destroy()


if __name__ == "__main__":
    # Instancio la clase Ejemplo
    w = Ejemplo()
    # Crea iteracion que maneja eventos en GTK
    Gtk.main()
